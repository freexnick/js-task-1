const students = [
    {
      name: "neo",
      grades: [
        { subject: "mchermetyveleba", grade: 60.2 },
        { subject: "chraKerva", grade: 57.3 },
        { subject: "parafsiqologia", grade: 72.4 },
        { subject: "valentobatmcodneoba", grade: 88 },
      ],
    },
    {
      name: "indiana",
      grades: [
        { subject: "mchermetyveleba", grade: 78.2 },
        { subject: "chraKerva", grade: 52.3 },
        { subject: "parafsiqologia", grade: 66.4 },
        { subject: "valentobatmcodneoba", grade: 80 },
      ],
    },
    {
        name: "severus",
        grades: [
            { subject: "mchermetyveleba", grade: 75.2 },
            { subject: "chraKerva", grade: 67.3 },
            { subject: "parafsiqologia", grade: 54.4 },
            { subject: "valentobatmcodneoba", grade: 90 },
        ],
    },
    {
      name: "aladin",
      grades: [
          { subject: "mchermetyveleba", grade: 80.2 },
          { subject: "chraKerva", grade: 52.3 },
          { subject: "parafsiqologia", grade: 68.4 },
          { subject: "valentobatmcodneoba", grade: 76 },
        ],
    },
]

const credits = {
    mchermetyveleba: 4,
    chraKerva: 2,
    parafsiqologia: 7,
    valentobatmcodneoba: 5,
}

const subjectMaxValue = 100
const sumOfCredits = Object.values(credits).reduce((acc, val) => acc + val)


const sumOfGrades = students.map(({name, grades}) =>{
    return {
        name,
        grade: grades
            .map(({grade}) => grade)
            .reduce((acc, val) => acc + val)
    }
})

const averageGrade = students.map(({name, grades}) =>{
    return{
        name,
        grade: grades
            .map(({grade}) => grade)
            .reduce((acc, val) => acc + val) / grades.length,
    }
})

const calculatePercent = num => (num / subjectMaxValue) * 100;

const gradePercentage = students.map(({name, grades}) =>{
    return{
        name,
        grade:grades.map(({grade}) => calculatePercent(grade))
    }
})

const calculateHighestValue = arr => {
    return arr.reduce((prev, cur) =>
    prev.grade > cur.grade ? prev : cur
    )
}

const calculateGpa =  grade => {
    if (grade >= 51 && grade <= 61) {
        return 0.5;
    }
    
    else if (grade >= 61 && grade <= 71) {
        return 1;
    }
    
    else if (grade >= 71 && grade <= 81) {
        return 2;
    }
    
    else if (grade >= 81 && grade <= 91) {
        return 3;
    }
    
    else if (grade >= 91 && grade <= 100) {
        return 4;
    }
    else {
        return grade
    }
}



const results = students.map(({ name, grades }) => {
    return {
      name,
      gpa:grades
          .map(({ subject, grade }) => credits[subject] * calculateGpa(grade))
          .reduce((acc, val) => acc + val) / sumOfCredits,
    };
});


console.log("highest overall:", calculateHighestValue(sumOfGrades))
console.log("highest average:", calculateHighestValue(averageGrade))
console.log("percentage:", gradePercentage)
console.log("gpa of each individual:", results)